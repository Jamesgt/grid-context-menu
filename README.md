# Grid context menu

Live demo https://jamesgt.gitlab.io/grid-context-menu

Code https://gitlab.com/Jamesgt/grid-context-menu

npm https://www.npmjs.com/package/@jamesgt/grid-context-menu

Usage from CDN
```html
<script src="https://unpkg.com/@jamesgt/grid-context-menu@2.4.0/GridContextMenu.js></script>
```

Discussion https://news.ycombinator.com/item?id=22609059
