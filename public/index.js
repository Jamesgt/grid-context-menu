import 'https://unpkg.com/@jamesgt/grid-context-menu@2.4.0/GridContextMenu.js'
// import '../GridContextMenu.js'

class Example {

  constructor($example) {
    this.$preview = document.createElement('div')
    this.$preview.classList.add('preview')
    this.$preview.innerHTML = 'Gerating preview...'
    this.$preview.title = 'preview'
    $example.parentNode.appendChild(this.$preview)

    $example.addEventListener('gcm-before-setup', (e) => this.menu = e.gcm)

    const $textArea = document.createElement('textarea')
    $textArea.value = html_beautify($example.outerHTML, {
      indent_size: 2,
      preserve_newlines: true,
      brace_style: 'collapse,preserve-inline',
    })
    $example.parentNode.appendChild($textArea)
    CodeMirror.fromTextArea($textArea, {
      styleActiveLine: true,
      matchBrackets: true,
      readOnly: true,
      theme: 'monokai',
      mode: 'text/html',
    })
  }

  showImagePreview() {
    this.menu.setPosition(6000, 6000)
    this.menu.show()
    html2canvas(this.menu.$element, {
      onrendered: canvas => {
        this.menu.hide()
        this.$preview.innerHTML = ''
        this.$preview.appendChild(canvas)
      }
    })
  }

}

const examples = Array.from(document.querySelectorAll('.example > nav.grid-context-menu')).map($example => new Example($example))

document.addEventListener('DOMContentLoaded', () => {
  examples.forEach(example => example.showImagePreview())
})
