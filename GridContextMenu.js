if (!document.querySelector('head > style[data-id="gcm"]')) {
  const gcmStyle = document.createElement('link')
  gcmStyle.dataset.id = 'gcm'
  gcmStyle.rel = 'stylesheet'
  gcmStyle.type = 'text/css'
  gcmStyle.href = 'https://unpkg.com/@jamesgt/grid-context-menu@2.4.0/GridContextMenu.css'
  // gcmStyle.href = '../GridContextMenu.css'
  document.querySelector('head').appendChild(gcmStyle)
}

export default class GridContextMenu {

  static get DEFAULT_SELECTOR() { return '.grid-context-menu' }

  visible = false

  constructor(options = { selector: GridContextMenu.DEFAULT_SELECTOR }) {
    switch (Object.prototype.toString.call(options)) {
      case '[object String]':
        options = { selector: options }
      // intentional fall through
      case '[object Object]':
        this.$element = document.querySelector(options.selector)
        break
      case '[object HTMLElement]':
        this.$element = options
        break
    }
    this.$parent = this.$element.parentNode
    this.$center = this.$element.querySelector('[data-pos="center"]')

    const theme = Array.from(this.$element.classList).find(c => c.startsWith('theme-'))?.replace('theme-', '')
    this.options = {
      ...{
        context: {},
        maxWidth: +this.$element.dataset['maxWidth'] || 4,
        itemWidth: this.$element.dataset['itemWidth'] || 'fit-content(1px)',
        rowHeight: this.$element.dataset['rowHeight'] || 'fit-content(1px)',
        defaultText: this.$element.dataset['defaultText'] || 'close',
        theme: theme || 'default',
        events: {},
      },
      ...options
    }

    const callbackFromOptions = this.options.events.beforeOpen
    this.options.events.beforeOpen = e => {
      if (this.options.restrict && !e.event.target.classList.contains(this.options.restrict)) {
        return true
      }
      e.context.$elem = e.event.target
      return callbackFromOptions?.(e)
    }

    this.isParentRelative = window.getComputedStyle(this.$parent).position == 'relative'

    this.$catcher = document.createElement('div')

    const beforeSetupEvent = new Event('gcm-before-setup')
    beforeSetupEvent.gcm = this
    this.$element.dispatchEvent(beforeSetupEvent)

    this.setup()
    this.setTheme(this.options.theme)

    this.$parent.addEventListener('contextmenu', this.open.bind(this))
  }

  setup() {
    this.hide()

    this.$element.addEventListener('click', e => e.stopPropagation())

    this.$catcher.classList.add('catcher')
    this.$catcher.style.cssText = 'width: 100%; position: absolute; z-index: 1000;' + (this.isParentRelative ? ' top: 0px;' : '')
    this.$catcher.addEventListener('click', e => {
      if (this.visible) {
        e.stopPropagation()
        this.hide()
      }
    })
    this.$catcher.addEventListener('contextmenu', e => {
      e.stopPropagation()
      e.preventDefault()
      this.hide()
      this.open(e)
    })

    const $actions = Array.from(this.$element.querySelectorAll('[data-action]'))
    this.$inputs = Array.from(this.$element.querySelectorAll('input[name]'))
    const $items = $actions.concat(this.$inputs)

    if (!this.options.actions) {
      this.options.actions = $items.map($item => $item.dataset['action'])
    }

    $items.forEach($item => {
      if (!$item.dataset['pos']) {
        $item.dataset['pos'] = 'bottom'
      }
      if (!$item.dataset['title']) {
        $item.dataset['title'] = $item.dataset['action'] || $item.name
      }
      if (this.options.itemWidth) {
        $item.style.width = this.options.itemWidth
      }
      if (this.options.rowHeight) {
        $item.style.height = this.options.rowHeight
      }
      $item.addEventListener('mouseover', e => this.setText(e.currentTarget.dataset['title']))
      $item.addEventListener('mouseleave', _ => this.setText(this.options.defaultText))
    })

    this.$inputs.forEach($item => {
      $item.addEventListener('input', e => {
        e.stopPropagation()
        const action = 'inputChange'
        const attribute = e.target.name
        const context = this.options.context
        const event = { action, attribute, context, inputValue: e.target.value }
        if (context.object) {
          context.object[attribute] = e.target.value
        } else {
          this.options.events['all']?.(event)
        }
        this.options.events[action]?.(event)
      })
      $item.addEventListener('click', e => e.stopPropagation()) // do not close context menu on an input click
    })

    $actions.forEach($item => {
      $item.addEventListener('click', e => {
        e.stopPropagation()
        const action = e.target.dataset['action']
        const event = { action, context: this.options.context }
        this.options.context.object?.[action]?.(this.options.context)
        this.options.events['all']?.(event)
        this.options.events[action]?.(event)
        this.hide()
      })
    })

    const top = Array.from(this.$element.querySelectorAll('[data-pos="top"]')).filter($item => this.options.actions.includes($item.dataset['action']))
    const bottom = Array.from(this.$element.querySelectorAll('[data-pos="bottom"]')).filter($item => this.options.actions.includes($item.dataset['action']))
    const width = Math.min(Math.max(top.length, bottom.length), this.options.maxWidth)
    const topHeight = Math.ceil(top.length / this.options.maxWidth)
    const centerHeight = this.$center ? 1 : 0
    const height = topHeight + centerHeight + Math.ceil(bottom.length / this.options.maxWidth)

    Object.assign(this.$element.style, {
      'grid-template-columns': `repeat(${width}, ${this.options.itemWidth})`,
      'grid-template-rows': `repeat(${height}, minmax(${this.options.rowHeight}, auto))`,
    })

    if (this.$center) {
      Object.assign(this.$center.style, {
        'grid-column': `1 / span ${width}`,
      })
    }
  }

  setText(text) {
    if (this.$center) {
      this.$center.innerHTML = text
    }
  }

  hide() {
    if (this.visible) {
      this.$parent.removeChild(this.$catcher)
    }
    this.visible = false
    this.$element.classList.add('hidden')
  }

  show() {
    if (!this.visible) {
      this.$parent.appendChild(this.$catcher)
    }
    this.visible = true
    const rect = this.$parent.getBoundingClientRect()
    this.$catcher.style.width = rect.width + 'px'
    this.$catcher.style.height = rect.height + 'px'

    this.setText(this.options.defaultText)
    this.$element.classList.remove('hidden')
  }

  setPosition(x, y) {
    Object.assign(this.$element.style, { left: x + 'px', top: y + 'px' })
  }

  open(event) {
    const openBlocked = this.options.events.beforeOpen({ event, context: this.options.context })
    if (openBlocked) {
      return
    }
    event?.preventDefault()
    event?.stopImmediatePropagation()

    this.$inputs.forEach($input => {
      const object = this.options.context.object
      const value = object?.[$input.name]
      if (object && value != null) {
        $input.value = value
      }
    })

    this.hide()
    const parentBoundingRect = this.$parent.getBoundingClientRect()
    this.show()
    const elementBoundingRect = this.$element.getBoundingClientRect()

    let x = Math.min(event.pageX, window.scrollX + parentBoundingRect.right - elementBoundingRect.width)
    let y = Math.min(event.pageY, window.scrollY + parentBoundingRect.bottom - elementBoundingRect.height)

    if (this.isParentRelative) {
      x -= parentBoundingRect.x + window.scrollX
      y -= parentBoundingRect.y + window.scrollY
    }
    this.setPosition(x, y)
  }

  setTheme(theme) {
    this.$element.classList.remove(`theme-${this.options.theme}`)
    this.$element.classList.add(`theme-${theme}`)
    this.options.theme = theme
  }

}

document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll(`${GridContextMenu.DEFAULT_SELECTOR}[data-auto]`).forEach($menu => new GridContextMenu($menu))
})

window.GridContextMenu = GridContextMenu
